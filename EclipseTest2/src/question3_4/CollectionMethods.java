/**
 * @author David Tran 1938381
 */

package question3_4;

import java.util.*;

public class CollectionMethods {

	public static Collection<Planet> getLargerThan(Collection<Planet> planets, double size){
		
		for(int count = 0; count < planets.size(); count++) {
			Iterator<Planet> planetIterator = planets.iterator();
			while(planetIterator.hasNext()) {
				Planet currentPlanet = planetIterator.next();
				if(currentPlanet.getRadius() < size) {
					planetIterator.remove();
				}
			}	
		}
		return planets;
	}
}