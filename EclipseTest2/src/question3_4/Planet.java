package question3_4;

/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet> {
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	
/*
 * My part for the exam
 *@author David Tran 1938381
 */
	/*
	 * Overriding equals() method
	 */
	public boolean equals(Object o) {
		if(this.planetarySystemName.equals(((Planet)o).getPlanetarySystemName()) && this.name.equals(((Planet)o).getName())) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/*
	 * Overriding hashCode() method
	 */
	public int hashCode() {
		String combinedPlanetNames = this.planetarySystemName + this.name;
		return combinedPlanetNames.hashCode();
	}
	
	/*
	 * Overriding compareTo() method to make Planet objects sortable
	 */
	public int compareTo(Planet p) {
		if(!(this.name.equals(p.name))) {
			return this.name.compareTo(p.name);
		}
		else {
			if(this.radius < p.radius) {
				return 1;
			}
			
			if(this.radius > p.radius) {
				return -1;
			}
			return 0;
		}
	}
	
}