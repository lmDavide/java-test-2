/**
 * @author David Tran 1938381
 */

package question2;

public interface Employee {
	
	double getWeeklyPay();
}
