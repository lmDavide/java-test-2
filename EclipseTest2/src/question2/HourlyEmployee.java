/**
 * @author David Tran 1938381
 */

package question2;

public class HourlyEmployee implements Employee {
	private double hoursOfWork; // Work hours of the employee
	private double hourlyPay; // $/hour of the employee

	/*
	 * HourlyEmployee Constructor
	 */
	public HourlyEmployee(double hoursOfWorkInput, double hourlyPayInput) {
		this.hoursOfWork = hoursOfWorkInput;
		this.hourlyPay = hourlyPayInput;
	}

	public double getWeeklyPay() {
		return (this.hoursOfWork * this.hourlyPay);
	}
}