/**
 * @author David Tran 1938381
 */

package question2;

public class UnionizedHourlyEmployee implements Employee {
	private double hoursOfWork; // Work hours of the employee
	private double hourlyPay; // $/hour of the employee
	private double maxHoursPerWeek; // Normal work hours of the employee ( if work hours more than max hours, then it becomes overtime...)
	private double overtimeRate; // overtime rate to the hourly pay of the employee for overtime hours
	
	/*
	 * UnionizedHourlyEmployee Constructor
	 */	
	public UnionizedHourlyEmployee(double hoursOfWorkInput, double hourlyPayInput, double maxHoursPerWeekInput, double overtimeRateInput) {
		this.hoursOfWork = hoursOfWorkInput;
		this.hourlyPay = hourlyPayInput;
		this.maxHoursPerWeek = maxHoursPerWeekInput;
		this.overtimeRate = overtimeRateInput;
	}
		
	public double getWeeklyPay() {
		if(this.hoursOfWork <= this.maxHoursPerWeek) {
			return (this.hoursOfWork * this.hourlyPay);
		}
		
		else { // this.hoursOfWork > this.maxHoursPerWeek 
			return (this.maxHoursPerWeek * this.hourlyPay + (this.hoursOfWork - this.maxHoursPerWeek) * (this.hourlyPay * this.overtimeRate));
			//      Normal work hours * hourlyPay + overtime Hours * overtime pay
		}
	}
	
}