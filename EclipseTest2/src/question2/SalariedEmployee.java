/**
 * @author David Tran 1938381
 */

package question2;

public class SalariedEmployee implements Employee{
	private double yearlySalary; // Yearly salary of the employee...
	
	/*
	 * SalariedEmployee Constructor
	 */
	public SalariedEmployee(double yearlySalaryInput) {
		this.yearlySalary = yearlySalaryInput;
	}

	public double getWeeklyPay() {
		return (yearlySalary/52);
	}

}
