/**
 * @author David Tran 1938381
 */

package question2;

public class PayrollManagement {

	public static void main(String[] args) {
		Employee[] myEmployees = new Employee[5];
		
		HourlyEmployee TotallyNotUnderpayed = new HourlyEmployee(168, 5);
		SalariedEmployee Bob = new SalariedEmployee(123456789);
		UnionizedHourlyEmployee Cat = new UnionizedHourlyEmployee(192, 10, 1 , 2);
		SalariedEmployee coolSecretary = new SalariedEmployee(210000000);
		HourlyEmployee MrIWasHereSinceTheStartOfTheCompany = new HourlyEmployee (72, 21);
		
		myEmployees[0] = TotallyNotUnderpayed;
		myEmployees[1] = Bob;
		myEmployees[2] = Cat;
		myEmployees[3] = coolSecretary;
		myEmployees[4] = MrIWasHereSinceTheStartOfTheCompany;
		
		System.out.println(getTotalExpenses(myEmployees));
		
	}
	
	/*
	 * Adding all of the weeklySalary of the employees together in order to get the weekly expenses of the company for its employees
	 */
	public static double getTotalExpenses(Employee[] employeeList) {
		double totalExpenses = 0;
		
		for(int count = 0; count < employeeList.length; count++) {
			totalExpenses = totalExpenses + employeeList[count].getWeeklyPay();
		}
		
		return totalExpenses;
	}
}
